// write your db connection code here
const mongoose = require('mongoose');
require('dotenv').config();
const options = { useNewUrlParser: true };

let connect, connection, connected;

if (!process.env.MONGO_URL) {
  throw new Error(`MONGO_URL environment variable is not set.\
This service is dependant on MongoDB server.`);
}

connection = () => mongoose.connect(process.env.MONGO_URL, options);
connect = () => connection().then(() => connected = true).catch(() => { });

mongoose.connection.on('disconnected', () => {
  connected = false;
  setTimeout(connect, 3000);
});

module.exports = {
  connect,
  get connected() { return connected; }
};
