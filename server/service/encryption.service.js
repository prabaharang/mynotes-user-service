const bcrypt = require('bcryptjs');

const ROUND_COUNT = 10;
const EncryptionService = { };

EncryptionService.encrypt = async text => {
  const salt = await bcrypt.genSalt(ROUND_COUNT);
  return await bcrypt.hash(text, salt);
};

EncryptionService.isValid = async (text, hash) => {
  return await bcrypt.compare(text, hash);
};

module.exports = EncryptionService;
