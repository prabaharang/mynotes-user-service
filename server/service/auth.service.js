const jwt = require('jsonwebtoken');

const AuthService = { };

AuthService.secret = '$ecret';

AuthService.signJWTToken = (payload, secret, expiresIn, callback) => {
  if (typeof callback === 'function') {
    jwt.sign(payload, secret, { expiresIn }, callback);
  } else {
    return new Promise((resolve, reject) => {
      jwt.sign(payload, secret, { expiresIn }, (err, token) => {
        if (err) reject(err);
        else resolve(token);
      });
    });
  }
};

AuthService.verifyJWTToken = (token, secret, callback) => {
  if (typeof callback === 'function') {
    jwt.verify(token, secret, (err, decoded) => {
      if (err) callback(err.message);
      else callback(null, decoded);
    });
  } else {
    return new Promise((resolve, reject) => {
      jwt.verify(token, secret, (err, decoded) => {
        if (err) reject(err.message);
        else resolve(decoded);
      });
    });
  }
};

module.exports = AuthService;
