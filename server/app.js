const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

const api = require('./api/v1');
const db = require('./db/connection');

require('dotenv').config();

// Connect DB
db.connect();

//write your logic here
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  db.connected ? next() : res.status(500).json({ message: 'Internal Server Error' });
});

app.use('/api/v1', api);

// 404
app.use((req, res, next) => res.status(404).send('Not Found'));

module.exports = app;
