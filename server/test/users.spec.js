require('chai').should();
const request = require('supertest');
const mongoose = require('mongoose');
const uuid = require('uuid/v1');
const jwt = require('jsonwebtoken');

const EncryptionService = require('../service/encryption.service');

const UserModel = require('../api/v1/users/users.model');

const app = require('../app');

// Master test suite
describe('User Test Suite', () => {
  let server;

  before(async () => {
    server = app.listen();
    await mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true });
  });
  
  after(async () => {
    await UserModel.deleteMany({ });
    mongoose.connection.close();
    server.close();
  });
  
  //  testsuite
  describe('Testing to register a user', () => {
    //  testcase
    it('Should handle a request to register a user', async () => {
      // Response body should have a key as userInfo which will hold 'username' value
      // status code = 201
      // response body will hold user.username

      const res = await request(server)
        .post('/api/v1/users/register')
        .send('username=username1&email=mailid@domain.com&password=password1');

      res.status.should.equal(201, 'Status should be 201 Created');
      res.body.should.have.property('userInfo');
      res.body.userInfo.should.equal('username1');
    });

    //  testcase
    it('Should handle invalid request when user tries to register with same username', async () => {
      //Response body should have a key as message which will hold value as 'username is already exist'
      // status code = 403
      // response body will hold an object with message key
      const user = new UserModel({
        username: 'same-username',
        email: 'mailid1@domain.com',
        password: 'password1'
      });

      await user.save();

      const res = await request(server)
        .post('/api/v1/users/register')
        .send({
          username: 'same-username',
          email: 'mailid2@domain.com',
          password: 'password1',
        });
      res.status.should.equal(403, 'Status should be 403 Forbidden');
      res.body.should.have.property('message');
      res.body.message.should.equal('username already exists');
    });

    //  testcase
    it('Should handle invalid request when user tries to register with same email', async () => {
      //Response body should have a key as message which will hold value as 'username is already exist'
      // status code = 403
      // response body will hold an object with message key
      const user = new UserModel({
        username: 'diff-username1',
        email: 'same_email@domain.com',
        password: 'password1'
      });

      await user.save();

      const res = await request(server)
        .post('/api/v1/users/register')
        .send({
          username: 'diff-username2',
          email: 'same_email@domain.com',
          password: 'password2',
        });
      res.status.should.equal(403, 'Status should be 403 Forbidden');
      res.body.should.have.property('message');
      res.body.message.should.equal('This email has already been registered');
    });
  });

  //  testsuite
  describe('Testing to login user', () => {

    before(async () => {
      const user = new UserModel({
        username: 'username2',
        password: await EncryptionService.encrypt('password2'),
        userId: uuid()
      });

      await user.save();
    });

    //  testcase
    it('Should handle a request to successfully login', async () => {
      //Response body should have a key as user which will hold username as a key and it will hold username value
      // status code = 200
      // response body will hold user.username
      // done();
      const res = await request(server)
        .post('/api/v1/users/login')
        .send({
          username: 'username2',
          password: 'password2'
        });
      
      res.status.should.equal(200, 'Status should be 200 OK');
      res.body.should.have.property('user');
      res.body.user.should.have.property('username');
      res.body.user.username.should.equal('username2');
      res.body.user.should.have.property('userId');
      res.body.should.have.property('token');
    });

    //  testcase
    it('Should handle a request to login with wrong password', async () => {
      //Response body should have a key as message which will hold value as 'Passwords is incorrect'
      // status code = 403
      // response body will hold an object with message key
      // done();
      const res = await request(server)
        .post('/api/v1/users/login')
        .send({
          username: 'username2',
          password: 'wrong-password'
        });
      
      res.status.should.equal(403, 'Status should be 403 Forbidden');
      res.body.should.have.property('message');
      res.body.message.should.equal('Passwords is incorrect', '\'message\' should be "Passwords is incorrect"');
    });

    //  testcase
    it('Should handle a request to login with wrong username', async () => {
      //Response body should have a key as message which will hold value as 'You are not registered user'
      // status code = 403
      // response body will hold an object with message key
      // done();
      const res = await request(server)
        .post('/api/v1/users/login')
        .send({
          username: 'wrong-username',
          password: 'wrong-password'
        });
      
      res.status.should.equal(403, 'Status should be 403 Forbidden');
      res.body.should.have.property('message');
      res.body.message.should.equal('You are not registered user', '\'message\' should be "You are not registered user"');
    });
  });

  describe('Testing to authenticate user', () => {
    let token, user;
    before(async () => {
      token = jwt.sign(
        { username: 'username1', userId: 'user1' },
        '$ecret',
        { expiresIn: 86400 }
      );

      await UserModel.deleteMany({ });

      user = new UserModel({
        username: 'username1',
        email: 'username1@domain.com',
        password: await EncryptionService.encrypt('password1'),
        userId: uuid()
      });

      await user.save();
    });

    it('Should handle request to authenticate valid user', async () => {
      const res = await request(server)
        .post('/api/v1/users/auth')
        .send({
          username: 'username1',
          password: 'password1'
        })
        .set('Authorization', `Bearer ${token}`);

      res.status.should.equal(200, 'Status should be 200 OK');
      res.body.should.have.property('user');
      res.body.user.should.have.property('userId');
      res.body.user.should.have.property('username');
      res.body.user.username.should.equal('username1');
      res.body.user.should.have.property('email');
      res.body.user.email.should.equal('username1@domain.com');
      res.body.should.have.property('isAuthenticated');
      res.body.isAuthenticated.should.be.true;
    });

    it('Should handle request to authenticate invalid user', async () => {
      const res = await request(server)
        .post('/api/v1/users/auth')
        .send({
          username: 'username999',
          password: 'password999'
        })
        .set('Authorization', `Bearer invalid`);

      res.status.should.equal(403, 'Status should be 403 Forbidden');
    });
  });
});
