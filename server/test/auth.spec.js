const expect = require('chai').expect;
const jwt = require('jsonwebtoken');

const { signJWTToken, verifyJWTToken } = require('../service/auth.service');

const secret = '$ecret';

describe('JWT Token test scenarios', function() {
  before(function(done) { done(); });
  after(function(done) { done(); });

  it('Assert signing & verification methods exists and are valid', function() {
    expect(signJWTToken).to.not.equal(undefined);
    expect(signJWTToken).to.not.equal(null);
    expect(typeof(signJWTToken)).to.equal('function');
    expect(signJWTToken.length).to.be.above(0, 'this method must have arguments');

    expect(verifyJWTToken).to.not.equal(undefined);
    expect(verifyJWTToken).to.not.equal(null);
    expect(typeof(verifyJWTToken)).to.equal('function');
    expect(verifyJWTToken.length).to.be.above(0, 'this method must have arguments');

    expect(signJWTToken).to.be.an('function');
  });

  it('sign a token with valid payload, signature, secret and expiry time', done => {
    signJWTToken({ username: 'username1', userId: '1' }, secret, 86400, (err, token) => {
      expect(err).to.equal(null);
      expect(typeof(token)).to.equal('string');
      done();
    });
  });
  it('verification of a valid signed token, must return same payload, which was passed', done => {
    const token = jwt.sign({ username: 'username2', userId: '2' }, secret, { expiresIn: 84600 });
    verifyJWTToken(token, secret, (err, payload) => {
      expect(err).to.equal(null);
      expect(payload).to.exist;
      expect(payload).to.have.property('username');
      expect(payload.username).to.equal('username2');
      expect(payload).to.have.property('userId');
      expect(payload.userId).to.equal('2');
      done();
    });
  });
  it('verification a expired token, must return with appropriate error', done => {
    const token = jwt.sign({ username: 'username2', userId: '2' }, secret, { expiresIn: '1ms' });
    setTimeout(() => {
      verifyJWTToken(token, secret, (err, payload) => {
        expect(err).to.exist;
        expect(payload).to.be.undefined;
        done();
      });
    }, 10);
  });
  it('verification a invalid, must return with appropriate error', done => {
    verifyJWTToken('invalid token', secret, (err, payload) => {
      expect(err).to.exist;
      expect(payload).to.be.undefined;
      done();
    });
  });
});
