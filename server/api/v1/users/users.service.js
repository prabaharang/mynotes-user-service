const EncryptionService = require('../../../service/encryption.service');
const AuthService = require('../../../service/auth.service');
const UserDAO = require('./users.dao');

const UserService = { };

const getSignedUser = async user => {
  const token = await AuthService.signJWTToken({
    username: user.username,
    userId: user.userId
  }, AuthService.secret, 86400);

  return {
    user: { username: user.username, userId: user.userId },
    token
  };
};

UserService.register = async user => {
  if (!user.username || !user.password || !user.email) throw new Error('INVALID_INPUT');
  const passwordHash = await EncryptionService.encrypt(user.password);
  const _user = await UserDAO.save({
    username: user.username,
    email: user.email,
    password: passwordHash
  });
  return { userInfo: _user.username };
};

UserService.login = async user => {
  const _user = await UserDAO.get(user.username);
  
  if (!_user) throw new Error('You are not registered user');
  const isValid = await EncryptionService.isValid(user.password, _user.password);

  if (!isValid) {
    throw new Error('Passwords is incorrect');
  }

  return await getSignedUser(_user);
};

UserService.get = async username => {
  let user = await UserDAO.get(username);
  if (!user) {
    throw new Error('User does not exist');
  }

  return {
    userId: user.userId,
    username: user.username,
    email: user.email
  };
};

module.exports = UserService;
