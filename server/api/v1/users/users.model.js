const db = require('mongoose');
const Schema = db.Schema;

const UserSchema = new Schema({
  userId: String,
  username: {
    type: String,
    validate: {
      validator: async username => {
        const users = await UserModel.find({ username });
        return users.length === 0;
      },
      message: 'USER_EXISTS'
    }
  },
  email: {
    type: String,
    validate: {
      validator: async email => {
        const users = await UserModel.find({ email });
        return users.length === 0;
      },
      message: 'EMAIL_EXISTS'
    }
  },
  password: String
});

const UserModel = db.model('User', UserSchema);

module.exports = UserModel;
