const router = require('express').Router();

const UserService = require('./users.service');
const AuthService = require('../../../service/auth.service');

router.route('/register').post(async (req, res) => {
  try {
    let _res = await UserService.register(req.body);
    res.status(201).json(_res);
  } catch (err) {
    if (err.message.includes('USER_EXISTS')) {
      res.status(403).json({ message: 'username already exists' });
    } else if (err.message.includes('EMAIL_EXISTS')) {
      res.status(403).json({ message: 'This email has already been registered' });
    } else if (err.message === 'INVALID_INPUT') {
      res.status(400).json({ message: 'username and password are required' });
    } else {
      res.status(500).json({ message: 'Server error' });
    }
  }
});

router.route('/login').post(async (req, res) => {
  try {
    let _res = await UserService.login(req.body);
    res.status(200).json(_res);
  } catch ({ message }) {
    res.status(403).json({ message });
  }
});

router.route('/auth').post(async (req, res) => {
  const authHeader = req.get('Authorization');
  if (!authHeader) res.status(403).send('Not authenticated');
  else {
    try {
      let token = authHeader.replace('Bearer ', '');
      let userAuth = await AuthService.verifyJWTToken(token, AuthService.secret);
      let user = await UserService.get(userAuth.username);
      res.status(200).send({ user, isAuthenticated: true });
    } catch (err) {
      res.status(403).send(err);
    }
  }
});

module.exports = router;
