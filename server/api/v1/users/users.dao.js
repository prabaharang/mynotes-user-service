const UserModel = require('./users.model');
const uuid = require('uuid/v1');

const UserDAO = { };

UserDAO.save = async userData => {
  userData.userId = uuid();
  const user = new UserModel(userData);
  return await user.save();
};

UserDAO.get = async username => {
  return await UserModel.findOne({ username });
};

module.exports = UserDAO;
