const router = require('express').Router();

const users = require('./users/users.route');

router.use('/users', users);

module.exports = router;
